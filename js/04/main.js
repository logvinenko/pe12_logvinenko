function sum(a, b) {
    return a + b;

}

/**
 * @desc Умножение двух чисед
 **@param {Number} a
 * @param {Number} b
 * @return {number}
 */
function multiply(a, b) {
    return a * b;
}

/**
 * @desc Подщитывает кол-во вхождений числа в строку
 * @param {string} str
 * @param {number} num
 * @return {number}
 */

function numInStr(str, num) {
    let counter = 0;

    if (!str) {
        return 0;
    }

    for (let i = 0; i < str.length; i++) {
        let s = str.charAt(i);

        if (s !== ' ' && +s === num) {
            counter++;
        }
    }
    return counter;
}

function diff(a, b) {
    return a - b;

}

function hockey(amountOfHits, gols) {
    if (!isFinite(amountOfHits) && !isFinite(gols)){
        return 0;
    }
    return gols / amountOfHits * 100;

}